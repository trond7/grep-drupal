# GREP-drupal

## About
This is a drupal instans that fetch and import all data from the GREP database.

[GREP is a database](http://www.udir.no/Stottemeny/English/Curriculum-in-English/) containg The Nowegian nation curricula managed by [The Norwegian Directorate for Education and Training](http://www.udir.no/Stottemeny/English/)

The curricula cover primary, lower secondary and upper secondary education and training.

The (LK06) National Curriculum for Knowledge Promotion in Primary and Secondary Education and Training comprises: 
 - The Core Curriculum and the Quality Framework 
 - Subject Curricula 
 - Distribution of teaching hours per subject

The technical description av the GREP database this tool import it's data from is descriped [here](http://grepwiki.udir.no) (including API and formats)

## Purpose
The main purpose is for me to have a personale backup of my GREP drupal site.
I decided to have it open if somene want to use it

With a local copy of GREP databse bulid with drupal it is easy to use drupal views to se dependencies between datasets and datatypes and attributes.


## Audience
 - Mainly Norwegian developers that works with GREP data
 - People that need to have a tool to analyse GREP data
 - People that need to keep track of changes in the GREP database


## Technical summary

 - It's It is a more or less standard drupal intance with very litle custom code.

 - The main feature is by using the drupal feeds module to import GREP data.

 - I personaly have set up this site with cron so that imports are done regulary

 - To install this you need a webserver with PHP and MYSQL
 - The ony thing you actually need from this reposistory is the [drush make file](https://bitbucket.org/trond7/grep-drupal/src/0f08303100faf5c314cbd6e2b19b4a3395b9aac1/backup/grep-drupal.make?at=master) ande the [mysql dump file](https://bitbucket.org/trond7/grep-drupal/src/6f644c99ecd4a44d14d5e8c9969e130f5d64ca8f/backup/mysql/?at=master). 
 - Install requiere three steps
	- Set up a webserver and a virtual host
 	- run a few drush command that download an enable drupal and required modules (from the drupal make file)
 	- Import the MYSQL database

## Installation
The installation is done by a few drush commands
Installation rquirements and install instructions are located in the [INSTALL](https://bitbucket.org/trond7/grep-drupal/src/31dcf22cca7886300163eba7e6786f5d9f841830/INSTALL.md?at=master&fileviewer=file-view-default) file
## About the author
 - I have the last few years developt several project based on drupal where I needed to import some data from the GREP database

 - I made this general purpose drupal site to keep track of changes and make custom views to analyse the data


## Licens / Warranty
=== WARNING: Use at your own risk! This script comes with no warranties, guarantees, or promises of any kind. ===

