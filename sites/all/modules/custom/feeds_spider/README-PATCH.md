# Description of the patch to this module

## Why
This module has a tiny patch made to make the spider work for the API 
data.udir.no/kl06/***.xml


The spider takes a xpath from a XML file/service and open all entries on that respond.
The GREP database API need to append fileformat to these url like this

http://data.udir.no/kl06/KD.xml
http://data.udir.no/kl06/KD.json

The spider module does not support appending fileformat in fetched URL.

## How

The patch adds only one line of code to the file FeedsSpider.inc
The patch file is includet in the module folder
If the FeedsSpider.inc file is downloadet/fetched/cloned from this repository the patch is already appended.
The patch file is only needed for upgrade or manualy download of file from drupal.org

~~~~
 foreach ($xml->xpath($source_config['xpath']) as $url) {
      $url = $url.".xml";  //this line is the only line added in patch @thh
      $urls[] = http_request_create_absolute_url((string) $url, $source_config['source']);
    }
~~~~



