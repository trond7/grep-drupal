## Installation

## Requierments
 - A webserver that can host the drupal CMS. LAMP/WAMP or similar.

(For Requierment for hosting av drupalsite se [drupal.org/requirements](https://www.drupal.org/requirements))

 - Drush 7.X command line tool for drupal 7

(For installation and ruquierments for drush se [drush on github](https://github.com/drush-ops/drush))


## Installing

### Alternative 1 
 - Make a installation folder to where tou want your site
 - Make a apace virtual host to that folder
 - Download these three files (the only files you actually need to sett up this site)
 	- Drush drupal MAKE file from [backup](https://bitbucket.org/trond7/grep-drupal/src/57b815f7e162f230cae3a615f3d98e597ea8be77/backup/?at=master)
 	- Latest Mysql dump file from [backup/mysql](https://bitbucket.org/trond7/grep-drupal/src/57b815f7e162f230cae3a615f3d98e597ea8be77/backup/mysql/?at=master)
 - Run these drush commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
drush make grep-drupal.make path/to/where/yout/Want/to/pu/The/root/folder 

drush sql-cli < path/to/the/mysql/file/Downloaded
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
   (The first command will download drupal 7 and all required drupal modules for this site)

   (The second import the mysql databse with all contents types, vocabulary and feed importers, views and more)
 - Download the file [/sites/all/modules/custom/feeds_spider/FeedsSpider.inc](https://bitbucket.org/trond7/grep-drupal/src/d3c53cc0b6fa2a1cb6c46bc4ea6c23f39455c69d/sites/all/modules/custom/feeds_spider/FeedsSpider.inc?at=master&fileviewer=file-view-default) and replace this ovverridden file with the same one the Make file downloadet.
 - Set a admin password for your drupal admin user: drush upwd --password="yourcustomPassword" "grep"
 - Log in to your site and flush the drupal cache



### Alternative 2
 - Download and install drupal 7.X from drupal.org
 - Download or clone this bitbucket repository in the root folder of the drupal site above
 - Import the mysql databse to your local mysql instance and add user priviliges to your database and update your drupal settings.php file
 - Make a virtual host in your webserver to the drupal site
 - Make the following symlinks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ln -s sites/all www/sites/all
ln -s www/sites/default/files files
ln -s www/sites/default/settings.php to_where_you_put_your_settingsfiles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - Set a admin password for your drupal admin user: drush upwd --password="yourcustomPassword" "grep"
 - Log in to your site and flush the drupal cache

## Updating

 - Download the newest versions of the MAKE file and the MYSQL file
 - Run these drush commands
 - drush sql-drop
 - drush sql-cli < path/to/the/mysql/file/Downloaded
 - drush make-update grep-drupal.make path/to/where/yout/Want/to/pu/The/root/folder 

## Custom code
There is actually only one line of code that is overridden/custom
[Explanation of this custom change](https://bitbucket.org/trond7/grep-drupal/src/307410353ffdee89f9405cfcd16c61f20054f095/sites/all/modules/custom/feeds_spider/README-PATCH.md?at=master&fileviewer=file-view-default)

## GREP Content sync
If you want to sync your drupal GREP site with the GREP database you need to set up a local cron job for your site
The site is alreaddy sett up so that the feeds importers for each content/taxonomy type then will import regulary
To change the frequensy of the sync just go to each importer and change uppdate frequency
The diffrent importeres has a number in their name and these indicate the the recomendet order these importers should be run.
The order are important due to witch content has a reference to each other.

