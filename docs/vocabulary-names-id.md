
Liste over vakabularer som bruke på GREP.hanssen.me

|Label|Machine name|vid|kommentar|
|Tags|tags|1||
|Trinn|trinn|2||
|Type Utdanningsprogram|type_utdanningsprogram|3||
|Fagkategorier|fagkategorier|4||
|Fagtype|grep_fagtype|5||
|Opplæringsnivå|grep_opplaeringsnivaa|6||
|Vurderingsform|grep_vurderingsform|7||
|Eksamensform|eksamensform|8||
|Type eksamensfag|type_eksamensfag|9||
|Merknader|merknader|10||
|Kompetansemål|kompetansemaal|11||
|Status|status|12||
|Merkelapper|merkelapper|13||
|Type programområde|type_programomraade|14||
|Fagområde|fagomraade|15||
|Type eksamensordning|type_eksamensordning|16||
|NUS2000 versjoner|nus2000_versjoner|17||
